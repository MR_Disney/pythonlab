import socket
import time
from time import strftime, gmtime

tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)          
tcp_socket.bind(("localhost", 777))                                     
tcp_socket.listen(1)                                                    

print('[ ! ] Підключення...')
connection, address = tcp_socket.accept()                               
print('[ ! ] Підключено')
print('[ ! ] Адреса клієнта: ', address)                                
print('__________________________________________________________________')
while True:
    data = connection.recv(1024)                                        
    newData = bytes.decode(data)                                       
    dataTime = strftime("%Y-%m-%d %H:%M:%S", gmtime())                  
    if not data or newData == "/close_connection":                      
        connection.close()                                              
        print("[ ! ] Connection closed")
        break
    else:
        print("[ Client -> Server ] : " + newData)                     
        newData = "[ Server -> Client ] : ("+dataTime+") "+newData      
        #time.sleep(5)                                                  
        print(newData)                                                  
        data = str.encode(newData)                                      
        connection.send(data)                                           
        print("")

tcp_socket.close()                                                      