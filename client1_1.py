import socket
import sys

tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)         
tcp_socket.connect(("localhost", 777))                                 

while True:
    data = input('  [ Client -> Server ] : ')                           
    if not data or data=="/close_connection":                           
        tcp_socket.close()                                              
        print("\n[ ! ] Connection closed")
        sys.exit(1)
    data = str.encode(data)                                             
    tcp_socket.send(data)                                              
    data = bytes.decode(data)
    data = tcp_socket.recv(1024)                                        
    print(data)                                                         
    print()

# /close_connection - команда для закриття з'єднання